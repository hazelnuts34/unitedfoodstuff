<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/unitedfoodstuff/admin/');
define('HTTP_CATALOG', 'http://localhost/unitedfoodstuff/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/unitedfoodstuff/admin/');
define('HTTPS_CATALOG', 'http://localhost/unitedfoodstuff/');

// DIR
define('DIR_APPLICATION', 'C:/xampp/htdocs/unitedfoodstuff/admin/');
define('DIR_SYSTEM', 'C:/xampp/htdocs/unitedfoodstuff/system/');
define('DIR_LANGUAGE', 'C:/xampp/htdocs/unitedfoodstuff/admin/language/');
define('DIR_TEMPLATE', 'C:/xampp/htdocs/unitedfoodstuff/admin/view/template/');
define('DIR_CONFIG', 'C:/xampp/htdocs/unitedfoodstuff/system/config/');
define('DIR_IMAGE', 'C:/xampp/htdocs/unitedfoodstuff/image/');
define('DIR_CACHE', 'C:/xampp/htdocs/unitedfoodstuff/system/cache/');
define('DIR_DOWNLOAD', 'C:/xampp/htdocs/unitedfoodstuff/system/download/');
define('DIR_UPLOAD', 'C:/xampp/htdocs/unitedfoodstuff/system/upload/');
define('DIR_LOGS', 'C:/xampp/htdocs/unitedfoodstuff/system/logs/');
define('DIR_MODIFICATION', 'C:/xampp/htdocs/unitedfoodstuff/system/modification/');
define('DIR_CATALOG', 'C:/xampp/htdocs/unitedfoodstuff/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'unitedfoodstuff');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
