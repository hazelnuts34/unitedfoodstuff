<?php

class ControllerModuleInformation extends Controller
{
    public function index()
    {
        $this->load->language('module/information');
        $this->load->language('information/information');
        if (isset($this->request->get['information_id'])) {
            $information_id = (int)$this->request->get['information_id'];
        } else {
            $information_id = 0;
        }
        $information_info = $this->model_catalog_information->getInformation($information_id);
        if (isset($this->request->get['information_id'])) {
            $data['heading_title'] = $information_info['title'];
        } else {

            $data['heading_title'] = $this->language->get('heading_title');
        }

        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_sitemap'] = $this->language->get('text_sitemap');

        $this->load->model('catalog/information');

        $data['informations'] = array();

        foreach ($this->model_catalog_information->getInformations() as $result) {
            $data['informations'][] = array(
                'title' => $result['title'],
                'href' => $this->url->link('information/information', 'information_id=' . $result['information_id'])
            );
        }

        $data['contact'] = $this->url->link('information/contact');
        $data['sitemap'] = $this->url->link('information/sitemap');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/information.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/information.tpl', $data);
        } else {
            return $this->load->view('default/template/module/information.tpl', $data);
        }
    }
}