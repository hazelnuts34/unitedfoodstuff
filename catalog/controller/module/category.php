<?php

class ControllerModuleCategory extends Controller
{
    public function index()
    {
        $this->load->language('module/category');
        if (!isset($this->request->get['route'])) {
            $this->load->language('information/information');
            if (isset($this->request->get['information_id'])) {
                $information_id = (int)$this->request->get['information_id'];
            } else {
                $information_id = 0;
            }
            $information_info = $this->model_catalog_information->getInformation($information_id);
            if (isset($this->request->get['information_id'])) {
                $data['heading_title'] = $information_info['title'];
            } else {

                $data['heading_title'] = $this->language->get('heading_title');
            }
            if (isset($this->request->get['path'])) {
                $parts = explode('_', (string)$this->request->get['path']);
            } else {
                $parts = array();
            }
            $data['page'] = 'info';
            $info_array = $this->model_catalog_information->getInformationSelect();
            foreach ($info_array as $info_data) {
                $data['info_cat'][] = array(
                    'href' => $this->url->link('information/information', 'information_id=' . $info_data['info_id']),
                    'information_id' => $info_data['info_id'],
                    'name' => $info_data['info_title']);
            }


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
            } else {
                return $this->load->view('unitedfoodstuff/template/module/category.tpl', $data);
            }
        } else {
            if (($this->request->get['route'] == 'information/information' || $this->request->get['route'] == 'common/home')) {
                $this->load->language('information/information');
                if (isset($this->request->get['information_id'])) {
                    $information_id = (int)$this->request->get['information_id'];
                } else {
                    $information_id = 0;
                }
                $information_info = $this->model_catalog_information->getInformation($information_id);
                if (isset($this->request->get['information_id'])) {
                    $data['heading_title'] = $information_info['title'];
                } else {

                    $data['heading_title'] = $this->language->get('heading_title');
                }
                if (isset($this->request->get['path'])) {
                    $parts = explode('_', (string)$this->request->get['path']);
                } else {
                    $parts = array();
                }
                $data['page'] = 'info';
                $info_array = $this->model_catalog_information->getInformationSelect();
                foreach ($info_array as $info_data) {
                    $data['info_cat'][] = array(
                        'href' => $this->url->link('information/information', 'information_id=' . $info_data['info_id']),
                        'information_id' => $info_data['info_id'],
                        'name' => $info_data['info_title']);
                }


                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
                } else {
                    return $this->load->view('unitedfoodstuff/template/module/category.tpl', $data);
                }
            } else if ($this->request->get['route'] == 'product/category' || $this->request->get['route'] == 'product/product') {
                $this->load->language('product/category');
                if (isset($this->request->get['path'])) {
                    $path = (int)$this->request->get['path'];
                } else {
                    $path = 0;
                }
                $data['heading_title'] = 'PRODUCTS';
                $data['path'] = $path;
                $data['name'] = 'All Products';
                $data['href'] = $this->url->link('product/category', 'path=' . $path);


                $data['page'] = 'products';
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/category.tpl', $data);
                }
            } else if ($this->request->get['route'] == 'information/contact' || $this->request->get['route'] == 'information/enquiry') {
                $this->load->language('product/category');
                if (isset($this->request->get['path'])) {
                    $path = (int)$this->request->get['path'];
                } else {
                    $path = 0;
                }
                $data['heading_title'] = 'PRODUCTS';
                $data['path'] = $path;
                $data['text_contact'] = 'Contact Us';
                $data['href_contact'] = $this->url->link('information/contact');
                $data['text_enquiry'] = 'Enquiry';
                $data['href_enquiry'] = $this->url->link('information/enquiry');

                $data['page'] = 'contact';
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/category.tpl', $data);
                }
            } else {
                $this->load->language('product/category');
                if (isset($this->request->get['path'])) {
                    $path = (int)$this->request->get['path'];
                } else {
                    $path = 0;
                }
                $product_info = $this->model_catalog_product->getProduct($path);
                if (isset($this->request->get['product_id'])) {
                    $data['heading_title'] = $product_info['title'];
                } else {

                    $data['heading_title'] = $this->language->get('heading_title');
                }
                if (isset($this->request->get['path'])) {
                    $parts = explode('_', (string)$this->request->get['path']);
                } else {
                    $parts = array();
                }

                if (isset($parts[0])) {
                    $data['category_id'] = $parts[0];
                } else {
                    $data['category_id'] = 0;
                }

                if (isset($parts[1])) {
                    $data['child_id'] = $parts[1];
                } else {
                    $data['child_id'] = 0;
                }

                $this->load->model('catalog/category');

                $this->load->model('catalog/product');

                $data['categories'] = array();

                $categories = $this->model_catalog_category->getCategories(0);

                foreach ($categories as $category) {
                    $children_data = array();

                    if ($category['category_id'] == $data['category_id']) {
                        $children = $this->model_catalog_category->getCategories($category['category_id']);

                        foreach ($children as $child) {
                            $filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);

                            $children_data[] = array(
                                'category_id' => $child['category_id'],
                                'name' => $child['name'],
                                'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
                            );
                        }
                    }

                    $filter_data = array(
                        'filter_category_id' => $category['category_id'],
                        'filter_sub_category' => true
                    );

                    $data['categories'][] = array(
                        'category_id' => $category['category_id'],
                        'name' => $category['name'],
                        'children' => $children_data,
                        'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                    );

                }
                $data['page'] = 'others';
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/category.tpl', $data);
                }
            }
        }

    }
}