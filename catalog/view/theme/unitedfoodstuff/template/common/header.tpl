<?php error_reporting(0);?>
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon"/>
    <?php } ?>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
    <link href="catalog/view/theme/unitedfoodstuff/stylesheet/stylesheet.css" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
</head>
<?php

?>
<body class="<?php echo $class; ?>">
<nav id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-sm-2">
                    <div id="logo">
                        <?php if ($logo) { ?>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>"
                                                            alt="<?php echo $name; ?>" class="img-responsive"/></a>
                        <?php } else { ?>
                        <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                </div>

                <div id="top-links" class="nav pull-right">
                    <ul class="list-inline">
                        <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>"
                               class="text-uppercase"><i class="fa fa-shopping-cart"></i> <span
                                        class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a>
                        </li>
                        <?php if ($logged) { ?>
                        <li><a href="<?php echo $account; ?>" class="text-uppercase"><?php echo $text_account; ?></a>
                        </li>
                        <li><a href="<?php echo $order; ?>" class="text-uppercase"><?php echo $text_order; ?></a></li>
                        <li><a href="<?php echo $transaction; ?>"
                               class="text-uppercase"><?php echo $text_transaction; ?></a></li>
                        <li><a href="<?php echo $download; ?>" class="text-uppercase"><?php echo $text_download; ?></a>
                        </li>
                        <li><a href="<?php echo $logout; ?>" class="text-uppercase"><?php echo $text_logout; ?></a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo $register; ?>" class="text-uppercase"><?php echo $text_register; ?></a>
                        </li>
                        <li>|</li>
                        <li><a href="<?php echo $login; ?>" class="text-uppercase"><?php echo $text_login; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>

                <?php if ($categories) { ?>

                <nav id="menu" class="navbar col-sm-8">
                    <div class="navbar-header"><span id="category"
                                                     class="visible-xs"><?php echo $text_category; ?></span>
                        <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse"
                                data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
                    </div>
                    <ul class="nav navbar-nav">


                        <li class="dropdown <?php echo $active_info; ?>"><a href="<?php echo $about; ?>" class="dropdown-toggle"
                                                data-toggle="dropdown"><?php echo $about_us ?></a>
                            <div class="dropdown-menu">
                                <div class="dropdown-inner">
                                    <ul class="list-unstyled">
                                        <li><a href="<?php echo $about ?>"><?php echo $about_us ?></a>
                                        <li><a href="<?php echo $director ?>"><?php echo $our_director ?></a>
                                        <li><a href="<?php echo $history ?>"><?php echo $united_history ?></a>
                                        <li><a href="<?php echo $future ?>"><?php echo $future_plans ?></a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </li>
                        <li class="<?php echo $active_product; ?>"><a href="<?php echo $producturl ?>"><?php echo $products ?></a>
                        <li class="<?php echo $active_contact; ?>"><a href="<?php echo $contacturl; ?>"><?php echo $contacts ?></a>
                    </ul>
                </nav>
                <div class="pull-right col-sm-2"><img src="image/chicken.png"></div>

                <?php } ?>
            </div>
        </div>
    </div>

</nav>
