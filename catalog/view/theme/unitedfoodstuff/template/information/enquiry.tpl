<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <h2 class="cat-title"><?php echo $heading_title; ?></h2>

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-10'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

            <div class="content-banner"><img
                        src="http://localhost/unitedfoodstuff/image/catalog/banner/contact-banner.jpg"></div>
            <h3 class="feel-contact"><?php echo strtoupper($feelfree_title);?></h3>

            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal enquiry">
                <fieldset>


                        <div class="col-md-6">
                            <div class="form-group required">
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name"
                                       class="form-control" placeholder="Name"/>
                                <?php if ($error_name) { ?>
                                <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email"
                                       class="form-control" placeholder="Email"/>
                                <?php if ($error_email) { ?>
                                <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                            <div class="form-group required">
                                <input type="text" name="telephone" value="<?php echo $telephone; ?>"
                                       id="input-telephone" class="form-control" placeholder="Mobile"/>
                                <?php if ($error_telephone) { ?>
                                <div class="text-danger"><?php echo $error_telephone; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                            <textarea name="enquiry" rows="7" id="input-enquiry"
                                      class="form-control"><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>
                            </div>
                        </div>

                    <?php if ($site_key) { ?>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
                            <?php if ($error_captcha) { ?>
                            <div class="text-danger"><?php echo $error_captcha; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </fieldset>
                <div class="buttons">
                        <input class="btn btn-united" type="submit" value="<?php echo $button_submit; ?>"/>
                </div>
            </form>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
