
<div class="list-group">
    <?php if($page=='info'){ ?>
    <?php foreach ($info_cat as $info) { ?>
    <a href="<?php echo $info['href']; ?>" class="list-group-item underline active"><?php echo $info['name']; ?></a>
    <?php } ?>
    <?php }else if($page=='products') { ?>
    <a href="<?php echo  $href; ?>" class="list-group-item active"><?php echo $name; ?></a>
    <?php }else if($page=='contact') { ?>
    <a href="<?php echo $href_contact; ?>" class="list-group-item underline active"><?php echo $text_contact; ?></a>
    <a href="<?php echo $href_enquiry; ?>" class="list-group-item underline active"><?php echo $text_enquiry; ?></a>
    <?php }else{ ?>
    <?php $count = $categories; $i=1;?>
    <?php foreach ($categories as $category) { ?>
    <?php if ($category['category_id'] == $category_id) { ?>
    <a href="<?php echo $category['href']; ?>"
       class="list-group-item underline active"><?php echo $category['name']; ?></a>
    <?php if ($category['children']) { ?>
    <?php foreach ($category['children'] as $child) { ?>
    <?php if ($child['category_id'] == $child_id) { ?>
    <a href="<?php echo $child['href']; ?>" class="list-group-item underline active">
        &nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
    <?php } else { ?>
    <a href="<?php echo $child['href']; ?>" class="list-group-item underline active">
        &nbsp;&nbsp;&nbsp;- <?php echo $child['name']; ?></a>
    <?php } ?>
    <?php } ?>
    <?php } ?>
    <?php } else { ?>
    <a href="<?php echo $category['href']; ?>"
       class="list-group-item <?php if($count==$i){ ?> underline <?php } ?>"><?php echo $category['name']; ?></a>
    <?php } ?>
    <?php

  $i++;} ?>
    <?php } ?>
</div>
