<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <h2 class="cat-title"><?php echo $heading_title; ?></h2>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-10'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>

    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($orders) { ?>
      <div class="table-responsive">
        <?php
          $i = 0;
          foreach ($orders as $order) { ?>
        <table class="table table-bordered table-hover">

          <thead>
            <tr>
              <td class="text-left" colspan="2"><?php echo $column_order_id; ?>: #<?php echo $order['order_id']; ?></td>
              <td class="text-center"><?php echo $column_status; ?>: <?php echo $order['status']; ?></td>
            </tr>
          </thead>
          <tbody>

            <tr>
              <td class="text-left"><?php echo $column_date_added.': '.$order['date_added']; ?><br/><?php echo $order['product_name']; ?></td>
              <td class="text-left"><?php echo $column_customer.': '.$order['name']; ?><br/> <?php echo $column_total.': '.$order['total'];  ?></td>
              <td class="text-center"><a href="<?php echo $order['href']; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-united-simple"><i class="fa fa-eye"></i></a><a onclick="reloadpage()" data-toggle="tooltip" title="<?php echo $button_refresh; ?>" class="btn btn-united-simple" style="cursor:pointer"><i class="fa fa-refresh"></i></a></td>
            </tr>

          </tbody>

        </table>
        <?php $i++; } ?>

      </div>
      <div class="text-right"><?php echo $pagination; ?></div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <div class="buttons clearfix">
        <div class="pull-right"><?php echo 'Showing item '. $i.' of '.$i; ?></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
<script>
  function reloadpage(){
    window.location.reload();
  }
</script>