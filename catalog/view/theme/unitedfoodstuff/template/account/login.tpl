<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
    <?php } ?>
    <h2 class="cat-title"><?php echo $login_title;?></h2>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?> login"><?php echo $content_top; ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="well">
                        <div class="well-left">
                            <h2><?php echo strtoupper($text_new_customer); ?></h2>

                            <p><?php echo $text_register_account; ?></p>
                            <a href="<?php echo $register; ?>"
                               class="btn btn-united"><?php echo $button_continue; ?></a>
                        </div>
                        <div class="well-right img">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="well">
                        <div class="well-left">
                        <h2><?php echo strtoupper($text_returning_customer); ?></h2>

                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="text" name="email" value="<?php echo $email; ?>"
                                       placeholder="Email" id="input-email"
                                       class="form-control"/>
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" value="<?php echo $password; ?>"
                                       placeholder="Password" id="input-password"
                                       class="form-control"/>

                                <a href="<?php echo $forgotten; ?>" class="link united-color"><?php echo $text_forgotten; ?></a></div>
                            <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-united"/>
                            <?php if ($redirect) { ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>"/>
                            <?php } ?>
                        </form>
</div>
                        <div class="well-right img2">
                        </div>

                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>